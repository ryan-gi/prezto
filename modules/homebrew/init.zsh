#
# Defines Homebrew aliases.

# Return if requirements are not found.
if [[ "$OSTYPE" != (darwin|linux)* ]]; then
  return 1
fi

#
# Aliases
#

# Homebrew
alias brewc='brew cleanup'
alias brewC='brew cleanup --force'
alias brewi='brew install'
alias brewI='brew info'
alias brewl='brew list'
alias brews='brew search'
alias brewu='brew update'
alias brewU='brew upgrade'
alias brewo='brew outdated'
alias brewx='brew remove'
alias brewh='brew home'

# Homebrew Cask
alias cask='brew cask'
alias caskc='brew cask cleanup --outdated'
alias caskC='brew cask cleanup'
alias caski='brew cask install'
alias caskI='brew cask info'
alias caskl='brew cask list'
alias casks='brew cask search'
alias caskx='brew cask uninstall'
alias caskh='brew cask home'
